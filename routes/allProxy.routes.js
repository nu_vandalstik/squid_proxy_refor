const express = require("express");
const router = express.Router();

var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing


const allproxy = require('../controllers/allProxy.controller')

router.get("/allproxy", allproxy.getAll)



// module.exports = router