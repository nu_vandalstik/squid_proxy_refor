const express = require("express");
const router = express.Router();

var bodyParser = require("body-parser"); // get body-parser
router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing


const proxy = require('../controllers/proxy.controller')

router.get("/proxy", proxy.getAllProxy)
router.post("/proxy", proxy.proxy)
router.put("/proxy/:id", proxy.updateProxy)
router.delete("/proxy/:id", proxy.deleteProxy)


module.exports = router