const db = require("../models");
const mongoose = require("mongoose")
const Proxy = db.proxy;
const allproxy = db.allProxy;
mongoose.set('useFindAndModify', false);
exports.proxy = (req, res) => {
  const proxy = new Proxy({
    server: req.body.server,
    username: req.body.username,
    password: req.body.password,
    port: req.body.port,
  });
  proxy.save(async (err, newproxy) => {
    if (err) {
      return res.status(500).send({
        message: err,
      });
    } 


// await allproxy.updateOne({_id:mongoose.Types.ObjectId("61031b049a5ed5a1547aa66b")
// },{$push : {api : mongoose.Types.ObjectId(newproxy._id) }})

    res.status(200).send({
      data: newproxy,
    });
  });
};

exports.getAllProxy = (req, res, next) => {
  Proxy.find()
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((err) => {
      next(err);
    });
};

exports.deleteProxy = (req, res, next) => {
  const proxyId = req.params.id;

  Proxy.findById(proxyId)
    .then( async(prox) => {
      if (!prox) {
        res.status(404).send({
          message: "Data Code Tidak Ditemukan",
        });
      }

      
      // await allproxy.updateMany(
      //   { },
      //   { $pull: { api: mongoose.Types.ObjectId(proxyId) } }
      // )
      

      return Proxy.findByIdAndRemove(proxyId);
    })
    .then((result) => {
      res.status(200).send({
        message: "Proxy Berhasil Di Hapus!",
        data: result,
      });
    })
    .catch((err) => {
      next(err);
    });
};

exports.updateProxy = (req, res) => {
  const proxyId = req.params.id;

  Proxy.findById(proxyId)
    .then((result) => {
      if (!result) {
        res.status(404).send({
          message: "Data Tidak Ditemukan!",
        });
      }
      (result.server = req.body.server),
        (result.username = req.body.username),
        (result.password = req.body.password),
        (result.port = req.body.port);
      return result.save();
    })
    .then((result) => {
      res.status(200).send({
        message: "Update Data Proxy Sukses !",
        data: result,
      });
    });
};
