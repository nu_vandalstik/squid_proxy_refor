const mongoose = require("mongoose");

const allProxy = mongoose.model(
  "allProxy",
  new mongoose.Schema({
    api: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Proxy",
      },
    ],
  }),
  "allProxy"
);

module.exports = allProxy;
