const mongoose = require("mongoose");

const Proxy = mongoose.model(
  "Proxy",
  new mongoose.Schema({
    server: String,
    username: String,
    password: String,
    port: String,
  }),
  "Proxy"
);

module.exports = Proxy;
