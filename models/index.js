const db = {};

db.proxy = require("../models/Proxy.models");
db.allProxy = require("../models/allProxy.model");

module.exports = db;
