const express = require("express");
const app = express();
const port = 3000;
const cors = require("cors");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const proxy = require("./routes/proxy.routes");
const allProxy = require("./routes/allProxy.routes");

var corsOptions = {
  origin: "*",
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Hello Cokkk!");
});

mongoose
  .connect(`mongodb://localhost:27017/Proxy`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    app.listen(port, () => {
      console.log(`Example app listening at http://localhost:${port}`);
    });
  })
  .catch((err) => console.log(err));

app.use(cors(corsOptions));
app.use("/v1", proxy);
// app.use("/v1", allProxy);
